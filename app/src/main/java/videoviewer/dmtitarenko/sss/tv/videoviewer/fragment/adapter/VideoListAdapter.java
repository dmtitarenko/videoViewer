package videoviewer.dmtitarenko.sss.tv.videoviewer.fragment.adapter;


import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import videoviewer.dmtitarenko.sss.tv.videoviewer.R;
import videoviewer.dmtitarenko.sss.tv.videoviewer.model.Video;
import videoviewer.dmtitarenko.sss.tv.videoviewer.model.VideoContainer;

/**
 * Class for RecyclerAdapter main video list
 */
public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.MyViewHolder> {

    private List<VideoContainer> videoContainerList;
    private Fragment fragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titleVideo;
        public ImageView coverVideo;

        public MyViewHolder(View view) {
            super(view);
            titleVideo = (TextView) view.findViewById(R.id.title_video);
            coverVideo = (ImageView) view.findViewById(R.id.cover_video);
        }
    }

    public VideoListAdapter(Fragment fragment, List<VideoContainer> videoContainerList) {
        this.fragment = fragment;
        this.videoContainerList = videoContainerList;
    }

    @Override
    public VideoListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VideoListAdapter.MyViewHolder holder, int position) {
        Video video = videoContainerList.get(position).getVideo();

        holder.titleVideo.setText(video.getVideoTitle());
        Glide.with(fragment).load(video.getImages().get(0).getImageUrl())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.coverVideo);
    }

    @Override
    public int getItemCount() {
        return videoContainerList.size();
    }
}
