package videoviewer.dmtitarenko.sss.tv.videoviewer.helper;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import videoviewer.dmtitarenko.sss.tv.videoviewer.constants.Constants;

/**
 * Created by dmti on 9/5/2016.
 */
public class FileHelper {

    private final static String LOG_TAG = FileHelper.class.getSimpleName();

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.exists()) {
            if(!file.mkdirs()) {
                Log.d(LOG_TAG, "Directory: " + file.getPath() + " not created");
            }
        } else {
            Log.d(LOG_TAG, "Directory: " + file.getPath() + " already exists");
        }
        return file;
    }

    /**
     *
     * @param imageView imageView to get Bitmap
     * @return boolean - true if sucess or false if not
     */
    public static boolean saveBitmapFromImageView(ImageView imageView) {
        Bitmap bitmap = ImageHelper.getBitmapFromImageView(imageView);
        if (FileHelper.isExternalStorageWritable()) {
            File imageDir = FileHelper.getAlbumStorageDir(Constants.IMAGE_ALBUM_NAME);

            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat ("yyyyMMddHHmmss");
            String imageName =  "img_" + sdf.format(date) + ".jpg";
            File imageFile = new File(imageDir, imageName);
            try {
                OutputStream outputStream = new FileOutputStream(imageFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outputStream);
                outputStream.flush();
                outputStream.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

}
