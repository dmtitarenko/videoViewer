package videoviewer.dmtitarenko.sss.tv.videoviewer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class represents Image
 */
public class Image {

    @SerializedName("url")
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public String toString() {
        return imageUrl;
    }
}
