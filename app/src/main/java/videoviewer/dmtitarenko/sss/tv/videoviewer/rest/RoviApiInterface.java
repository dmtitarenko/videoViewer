package videoviewer.dmtitarenko.sss.tv.videoviewer.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import videoviewer.dmtitarenko.sss.tv.videoviewer.model.FilterBrowserResponse;

/**
 * API for interact with ROVI API
 */
public interface RoviApiInterface {

    /**
     *
     * @param entityType type of entity for movies should be 'movie'
     * @param filter filter of results, example: 'releaseyear:2002'
     * @param include field using to indicate include some fields to response, example: 'releaseyear:2002'
     * @param size number of results in response
     * @param country country, supported only 'US'
     * @param language language, supported only 'en'
     * @param apiKey API key for ROVI service
     * @param sig SIG generated from: <a href='http://developer.rovicorp.com/siggen'>sig Parameter Generator</a>
     * @param format format of response, must be 'json'
     * @return
     */
    @GET("video/filterbrowse")
    Call<FilterBrowserResponse> getFilterBrowseMovies(@Query("entitytype") String entityType,
                                                      @Query("filter") String filter,
                                                      @Query("include") String include,
                                                      @Query("size") int size,
                                                      @Query("country") String country,
                                                      @Query("language") String language,
                                                      @Query("apikey") String apiKey,
                                                      @Query("sig") String sig,
                                                      @Query("format") String format);

}
