package videoviewer.dmtitarenko.sss.tv.videoviewer.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Class represents 'video' JSON Array in response
 */
public class Video {

    @SerializedName("ids")
    private VideoIds videoIds;

    @SerializedName("masterTitle")
    private String videoTitle;

    @SerializedName("images")
    private List<Image> images = new ArrayList<>();

    transient private String videoStreamUrl = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    @Override
    public String toString() {
        return videoTitle;
    }

    public VideoIds getVideoIds() {
        return videoIds;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public List<Image> getImages() {
        return images;
    }

    public String getVideoStreamUrl() {
        return videoStreamUrl;
    }
}
