package videoviewer.dmtitarenko.sss.tv.videoviewer.helper;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;

/**
 * Created by dmti on 9/5/2016.
 */
public class PermissionHelper {

    public final static String PERMISSION_PREFERENCES = "VideoViewerPermissions";
    public final static int PERMISSION_REQUEST_CODE = 200;

    public final static String WRITE_EXT_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";

    public static boolean isDeviceMarshmallow() {
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * Method to ask one to get permission
     *
     * @param activity current activity
     * @param permission permission to ask
     * @return result of getting permission
     */
    public static boolean askForPermissionOnce(Activity activity, String permission) {
        String[] permArray = {permission};
        if (!hasPermission(activity, permission) && shouldAsk(activity, permission)) {
            activity.requestPermissions(permArray, PERMISSION_REQUEST_CODE);
            markAsAsked(activity, permission);
        }
        return hasPermission(activity, permission);
    }

    public static boolean hasPermission(Activity activity, String permission) {
        if (isDeviceMarshmallow()) {
            return(activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
        }
        return true;
    }

    private static boolean shouldAsk(Activity activity, String permission) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(PERMISSION_PREFERENCES, 0);
        return (sharedPreferences.getBoolean(permission, true));
    }

    private static void markAsAsked(Activity activity, String permission) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(PERMISSION_PREFERENCES, 0);
        sharedPreferences.edit().putBoolean(permission, false).apply();
    }

}
