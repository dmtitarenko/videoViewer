package videoviewer.dmtitarenko.sss.tv.videoviewer.fragment;


import android.media.MediaCodec;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecSelector;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;

import videoviewer.dmtitarenko.sss.tv.videoviewer.R;
import videoviewer.dmtitarenko.sss.tv.videoviewer.fragment.adapter.VideoCoverAdapter;
import videoviewer.dmtitarenko.sss.tv.videoviewer.model.Video;

public class VideoDetailFragment extends Fragment {

    private static final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private static final int BUFFER_SEGMENT_COUNT = 256;

    private Video video;
    private ExoPlayer exoPlayer;
    private long exoPlayerPosition = 0;
    private LinearLayout videoCoversContainerLinearLayout;

    public void setVideo(Video video) {
        this.video = video;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);

        View rootView = inflater.inflate(R.layout.video_detail, null);

        ImageView videoCoverImageView = (ImageView) rootView.findViewById(R.id.cover_video_detail_static);
        Glide.with(this).load(video.getImages().get(0).getImageUrl())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(videoCoverImageView);

        TextView videoCoverTitleTextView = (TextView) rootView.findViewById(R.id.title_video_detail_static);
        videoCoverTitleTextView.setText(video.getVideoTitle());

        final ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.cover_video_view_pager);
        viewPager.setAdapter(new VideoCoverAdapter(getChildFragmentManager(), video.getImages()));

        Button nextButton = (Button) rootView.findViewById(R.id.next_cover_video_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int videoCoverSize = video.getImages().size();
                int currentPosition = viewPager.getCurrentItem();
                Log.d("viewPager", "current: " + currentPosition);
                if (currentPosition < videoCoverSize) {
                    Log.d("viewPager", "next set: " + (currentPosition + 1));
                    viewPager.setCurrentItem(currentPosition + 1);
                }
            }
        });

        Button prevButton = (Button) rootView.findViewById(R.id.prev_cover_video_button);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentPosition = viewPager.getCurrentItem();
                Log.d("viewPager", "current: " + currentPosition);
                if (currentPosition > 0) {
                    Log.d("viewPager", "prev set: " + (currentPosition - 1));
                    viewPager.setCurrentItem(currentPosition - 1);
                }
            }
        });

        videoCoversContainerLinearLayout = (LinearLayout) rootView.findViewById(R.id.video_covers_container);
        videoCoversContainerLinearLayout.setVisibility(View.GONE);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videoCoversContainerLinearLayout.getVisibility() == View.VISIBLE) {
                    videoCoversContainerLinearLayout.setVisibility(View.GONE);
                    exoPlayer.setPlayWhenReady(true);

                }
            }
        });

        videoCoverImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videoCoversContainerLinearLayout.getVisibility() != View.VISIBLE) {
                    videoCoversContainerLinearLayout.setVisibility(View.VISIBLE);
                    exoPlayer.setPlayWhenReady(false);
                }
            }
        });

        prepareExoPlayerForPlayback(rootView);
        if (exoPlayerPosition > 0) {
            exoPlayer.seekTo(exoPlayerPosition);
        }
        exoPlayer.setPlayWhenReady(true);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        exoPlayerPosition = exoPlayer.getCurrentPosition();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        exoPlayer.release();
    }

    private void prepareExoPlayerForPlayback(View rootView) {
        Allocator allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        Uri uri = Uri.parse(video.getVideoStreamUrl());
        DataSource dataSource = new DefaultUriDataSource(getActivity().getApplicationContext(), "VideoViewer");
        ExtractorSampleSource sampleSource = new ExtractorSampleSource(uri, dataSource, allocator,
                BUFFER_SEGMENT_COUNT * BUFFER_SEGMENT_SIZE);

        TrackRenderer videoRenderer = new MediaCodecVideoTrackRenderer(getActivity().getApplicationContext(),
                sampleSource, MediaCodecSelector.DEFAULT, MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT);
        TrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource, MediaCodecSelector.DEFAULT);

        exoPlayer = ExoPlayer.Factory.newInstance(2);
        exoPlayer.prepare(videoRenderer, audioRenderer);

        SurfaceView surfaceView = (SurfaceView) rootView.findViewById(R.id.surface_view);
        exoPlayer.sendMessage(videoRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, surfaceView.getHolder().getSurface());
    }
}
