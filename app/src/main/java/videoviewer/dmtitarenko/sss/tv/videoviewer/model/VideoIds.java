package videoviewer.dmtitarenko.sss.tv.videoviewer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for storing cosmoId field of FilterBrowse request
 */
public class VideoIds {

    @SerializedName("cosmoId")
    private String videoId;

    @Override
    public String toString() {
        return videoId;
    }

    public String getVideoId() {
        return videoId;
    }
}
