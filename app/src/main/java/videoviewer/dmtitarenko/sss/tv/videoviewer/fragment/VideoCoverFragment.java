package videoviewer.dmtitarenko.sss.tv.videoviewer.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import videoviewer.dmtitarenko.sss.tv.videoviewer.R;
import videoviewer.dmtitarenko.sss.tv.videoviewer.helper.FileHelper;
import videoviewer.dmtitarenko.sss.tv.videoviewer.helper.PermissionHelper;
import videoviewer.dmtitarenko.sss.tv.videoviewer.model.Image;

public class VideoCoverFragment extends Fragment {

    private String mVideoCoverUrl;

    /**
     * Create a new instance of VideoCoverFragment, providing "videoCoverUrl"
     * as an argument. Using String to avoid implementing Parceable interface
     */
    public static VideoCoverFragment newInstance(Image videoCover) {
        VideoCoverFragment f = new VideoCoverFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("videoCoverUrl", videoCover.getImageUrl());
        f.setArguments(args);

        return f;
    }

    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVideoCoverUrl = getArguments() != null ? getArguments().getString("videoCoverUrl") : "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("URL", mVideoCoverUrl);
        View rootView = inflater.inflate(R.layout.video_cover_detail_item, container, false);
        final ImageView videoCoverItemImageView = (ImageView) rootView.findViewById(R.id.cover_video_item_in_view_pager);

        videoCoverItemImageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (PermissionHelper.askForPermissionOnce(getActivity(), PermissionHelper.WRITE_EXT_STORAGE)) {
                    FileHelper.saveBitmapFromImageView(videoCoverItemImageView);
                    return true;
                } else {
                    return false;
                }
            }
        });

        Glide.with(this).load(mVideoCoverUrl)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(videoCoverItemImageView);

        return rootView;
    }

}
