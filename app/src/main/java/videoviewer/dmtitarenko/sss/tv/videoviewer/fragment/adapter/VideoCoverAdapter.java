package videoviewer.dmtitarenko.sss.tv.videoviewer.fragment.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import videoviewer.dmtitarenko.sss.tv.videoviewer.fragment.VideoCoverFragment;
import videoviewer.dmtitarenko.sss.tv.videoviewer.model.Image;

/**
 * Created by dmti on 9/2/2016.
 */
public class VideoCoverAdapter extends FragmentPagerAdapter {

    private List<Image> videoCoverList;

    public VideoCoverAdapter(FragmentManager fm, List<Image> videoCoverList) {
        super(fm);
        this.videoCoverList = videoCoverList;
    }

    @Override
    public Fragment getItem(int position) {
        return VideoCoverFragment.newInstance(videoCoverList.get(position));
    }

    @Override
    public int getCount() {
        return videoCoverList.size();
    }
}
