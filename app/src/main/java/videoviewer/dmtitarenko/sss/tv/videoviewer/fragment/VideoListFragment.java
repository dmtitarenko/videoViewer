package videoviewer.dmtitarenko.sss.tv.videoviewer.fragment;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import videoviewer.dmtitarenko.sss.tv.videoviewer.R;
import videoviewer.dmtitarenko.sss.tv.videoviewer.constants.Constants;
import videoviewer.dmtitarenko.sss.tv.videoviewer.fragment.adapter.VideoListAdapter;
import videoviewer.dmtitarenko.sss.tv.videoviewer.fragment.decoration.DividerItemDecoration;
import videoviewer.dmtitarenko.sss.tv.videoviewer.fragment.listener.RecyclerTouchListener;
import videoviewer.dmtitarenko.sss.tv.videoviewer.model.FilterBrowserResponse;
import videoviewer.dmtitarenko.sss.tv.videoviewer.model.SearchResponse;
import videoviewer.dmtitarenko.sss.tv.videoviewer.model.Video;
import videoviewer.dmtitarenko.sss.tv.videoviewer.rest.RoviApiClient;
import videoviewer.dmtitarenko.sss.tv.videoviewer.rest.RoviApiInterface;

/**
 * Class for Video Items List Fragment
 */
public class VideoListFragment extends Fragment {

    private final static String LOG_TAG = VideoListFragment.class.getSimpleName();
    private SearchResponse searchResponse = null;
    private RecyclerView recyclerView;
    private RecyclerTouchListener recyclerTouchListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        recyclerTouchListener = new RecyclerTouchListener(getActivity().getApplicationContext(),
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Video video = searchResponse.getResults().get(position).getVideo();

                VideoDetailFragment videoDetailFragment = new VideoDetailFragment();
                videoDetailFragment.setVideo(video);

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.addToBackStack(null).replace(R.id.fragment_container, videoDetailFragment);
                fragmentTransaction.commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.video_list, null);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.video_recycler_view);
        recyclerView.setLayoutManager(getLinearLayoutManager());
        recyclerView.addItemDecoration(getDividerItemDecoration());
        recyclerView.addOnItemTouchListener(recyclerTouchListener);

        if (searchResponse == null) {
            updateRecyclerViewFromRoviResponse();
        } else {
            recyclerView.setAdapter(new VideoListAdapter(VideoListFragment.this, searchResponse.getResults()));
        }

        return rootView;
    }

    private LinearLayoutManager getLinearLayoutManager() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        } else {
            return new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        }
    }

    private DividerItemDecoration getDividerItemDecoration() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return new DividerItemDecoration(getActivity(), LinearLayoutManager.HORIZONTAL);
        } else {
            return new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        }
    }

    private void updateRecyclerViewFromRoviResponse() {
        if (recyclerView != null) {
            RoviApiInterface roviApiService = RoviApiClient.getClient().create(RoviApiInterface.class);
            Call<FilterBrowserResponse> call = roviApiService.getFilterBrowseMovies(
                    "movie", "releaseyear%3A2002", "images", 30, "US", "en", Constants.API_KEY, Constants.SIG, Constants.RESPONSE_FORMAT);

            call.enqueue(new Callback<FilterBrowserResponse>() {
                @Override
                public void onResponse(Call<FilterBrowserResponse> call, Response<FilterBrowserResponse> response) {
                    try {
                        searchResponse = response.body().getSearchResponse();
                        recyclerView.setAdapter(new VideoListAdapter(VideoListFragment.this, searchResponse.getResults()));
                    } catch (NullPointerException ex) {
                        Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                                "Getting response from Rovi failed (null response)", Toast.LENGTH_SHORT);
                        toast.show();
                        Log.e(LOG_TAG, ex.toString());
                    }
                }

                @Override
                public void onFailure(Call<FilterBrowserResponse> call, Throwable t) {
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                            "Getting response from Rovi failed", Toast.LENGTH_SHORT);
                    toast.show();
                    Log.e(LOG_TAG, t.toString());
                }
            });
        } else {
            Log.e(LOG_TAG, "RecyclerView is not created");
        }
    }
}
