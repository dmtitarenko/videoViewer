package videoviewer.dmtitarenko.sss.tv.videoviewer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class represents "searchResponse" JSON Object
 */
public class FilterBrowserResponse {

    @SerializedName("searchResponse")
    private SearchResponse searchResponse;

    public SearchResponse getSearchResponse() {
        return searchResponse;
    }
}
