package videoviewer.dmtitarenko.sss.tv.videoviewer;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import videoviewer.dmtitarenko.sss.tv.videoviewer.fragment.VideoListFragment;

public class MainActivity extends FragmentActivity {

    private FragmentManager fragmentManager;
    private VideoListFragment videoListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);

        fragmentManager = getSupportFragmentManager();

        videoListFragment = new VideoListFragment();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (savedInstanceState == null) {
            fragmentTransaction.add(R.id.fragment_container, videoListFragment);
            fragmentTransaction.commit();
        }
    }

}
