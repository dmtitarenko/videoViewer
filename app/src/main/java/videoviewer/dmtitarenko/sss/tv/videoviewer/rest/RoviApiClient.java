package videoviewer.dmtitarenko.sss.tv.videoviewer.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class for Rovi REST API client
 */
public class RoviApiClient {

    public static final String BASE_URL = "http://api.rovicorp.com/search/v2.1/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
