package videoviewer.dmtitarenko.sss.tv.videoviewer.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Class represents "results" JSON Array from FilterBrowser requests response
 */
public class SearchResponse {

    @SerializedName("results")
    private List<VideoContainer> results = new ArrayList<>();

    public List<VideoContainer> getResults() {
        return results;
    }
}
