package videoviewer.dmtitarenko.sss.tv.videoviewer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class represents "video" JSON Object form FilterBrowser requests response
 */
public class VideoContainer {

    @SerializedName("video")
    private Video video;

    @Override
    public String toString() {
        return video.toString();
    }

    public Video getVideo() {
        return video;
    }
}
